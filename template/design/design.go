package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

// Define your API!

var _ = API("API name", func() {
	Title
	Description
	Version
	TermsofService
	Contact(func() {
		Name("Name")
		Email("Email")
		URL("URL")
	})
	License(func() {
		Name("Name")
		URL("URL")
	})
	Docs(func() {
		Description("Description")
		URL("URL")
	})
	Host("Host")
	Scheme("Scheme")
	BasePath("BasePath")
	Params(func() {
		Param("Param")
	})
	Security("JWT")
	Origin("", func() {
		Headers()
		Methods()
		Expose()
		MaxAge(600)
		Credentials()

	})
	Consumes("application/json")
	Produces("application/json", func() {
		Package("github.com/goadesign/goa/encoding/json")
	})
	ResponseTemplate("static", func() {
		Desctiption
		Status(404)
		MediaType("application/json")
	})
	ResponseTemplate("dynamic", func(arg1, arg2 string) {
		Description(arg1)
		Status(200)
		MediaType(arg2)
	})
})

var _ = Resource("resource1", func() {
	Description("description")
	DefaultMedia("DefaultMedia")
	BasePath("BasePath")
	Parent("Parent")
	CononicalActionName("")
	UseTrait("")
	Origin("", func() {
		Headers("")
		Methods("GET", "POST")
		Expose("")
		MaxAge(600)
		Credentials()
	})
	Response(Unauthorized, ErrorMedia)
	Response(BadRequest, ErrorMedia)
	Action("show", func() {
		// ... Action DSL
	})
})

var _ = MediaType("application/vnd.goa.example.bottle", func() {
    Description("A bottle of wine")
    TypeName("BottleMedia")         // Override default generated name
    ContentType("application/json") // Override default Content-Type header value
    Attributes(func() {
        Attribute("id", Integer, "ID of bottle")
        Attribute("href", String, "API href of bottle")
        Attribute("account", Account, "Owner account")
        Attribute("origin", Origin, "Details on wine origin")
        Links(func() {
            Link("account")         // Defines link to Account media type
            Link("origin", "tiny")  // Set view used to render link if not "link"
        })
        Required("id", "href")
    })
    View("default", func() {
        Attribute("id")
        Attribute("href")
        Attribute("links")          // Renders links
    })
    View("extended", func() {
        Attribute("id")
        Attribute("href")
        Attribute("account")        // Renders account inline
        Attribute("origin")         // Renders origin inline
        Attribute("links")          // Renders links
    })
 })