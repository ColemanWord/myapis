package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = API("adder", func() {
	Title("the adder api")
	Description("add yo numbas")
	Host("localhost:8080")
	Scheme("http")
})

var _ = Resource("operands", func() {
	Action("add", func() {
		Routing(GET("add/:left/:right"))
		Description("add left and right params")
		Params(func() {
			Param("left", Integer, "left operand")
			Param("right", Integer, "right operand")
		})
		Response(OK, "text/plain")
	})
})
